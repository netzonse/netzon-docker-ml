# Netzon Docker ML Toolkit

Reference: [antoniosap/netzon-docker-ml](https://github.com/antoniosap/netzon-docker-ml)

## Building

```
sudo docker build -t netzonse/netzon-docker-ml:latest .
```

## Run in isolation

### Powershell

Where shared path is `C:/shared`.

```powershell
docker run -d `
   -it `
   --net=host `
   --env="DISPLAY" `
   --volume="$HOME/.Xauthority:/root/.Xauthority:rw" `
   --name netzon-docker-ml `
   --mount type=bind,source=C:/shared,target=/mnt `
   netzonse/netzon-docker-ml:latest bash
```

Connecting to the VM.

```powershell
docker attach netzon-docker-ml
```