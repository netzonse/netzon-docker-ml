FROM tensorflow/tensorflow:latest-py3

WORKDIR /
ENV OPENCV_VERSION="4.3.0"
ENV DEBIAN_FRONTEND="noninteractive"
ENV QT_X11_NO_MITSHM=1

RUN echo "Asia/Manila" | tee /etc/timezone \
  && apt-get update \
  && apt-get install -y tzdata \
  && ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime \
  && dpkg-reconfigure --frontend noninteractive tzdata \
  \
  && apt-get update \
  && apt-get install -y \
    build-essential \
    cmake \
    git \
    wget \
    unzip \
    yasm \
    pkg-config \
    libswscale-dev \
    libtbb2 \
    libtbb-dev \
    libjpeg-dev \
    libpng-dev \
    libtiff-dev \
    libavformat-dev \
    libpq-dev \
    libqt4-dev \
    qt5-default \
    nano \
    vim \
    mc \
    python3-tk \
    xfce4 \
    xfce4-goodies \
    tightvncserver \
    net-tools \
  \
  && rm -rf /var/lib/apt/lists/* \
  && pip install --upgrade pip \
  && pip install \
    numpy \
    ipython \
    scipy \
    jupyter \
    moviePy \
    imutils \
    keras \
    matplotlib \
    scikit-image \
    imgaug \
    cython \
    pycocotools \
  \    
  && wget https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip \
    && unzip ${OPENCV_VERSION}.zip \
    && rm ${OPENCV_VERSION}.zip \
  \
  && wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip \
  && unzip ${OPENCV_VERSION}.zip \
  && mkdir /opencv-${OPENCV_VERSION}/cmake_binary \
  && cd /opencv-${OPENCV_VERSION}/cmake_binary \
  && cmake -DBUILD_TIFF=ON \
    -DBUILD_opencv_java=OFF \
    -DOPENCV_EXTRA_MODULES_PATH=/opencv_contrib-${OPENCV_VERSION}/modules \
    -DWITH_CUDA=OFF \
    -DWITH_OPENGL=ON \
    -DWITH_OPENCL=ON \
    -DWITH_IPP=ON \
    -DWITH_TBB=ON \
    -DWITH_EIGEN=ON \
    -DWITH_V4L=ON \
    -DWITH_QT=ON \
    -DBUILD_TESTS=OFF \
    -DBUILD_PERF_TESTS=OFF \
    -DCMAKE_BUILD_TYPE=RELEASE \
    -DCMAKE_INSTALL_PREFIX=$(python -c "import sys; print(sys.prefix)") \
    -DPYTHON_EXECUTABLE=$(which python) \
    -DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
    -DPYTHON_PACKAGES_PATH=$(python -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
    .. \
  && make -j`nproc` install \
  && rm /${OPENCV_VERSION}.zip \
  && rm -r /opencv-${OPENCV_VERSION} 

RUN groupadd -r jupyter \
  && useradd --no-log-init -r -g jupyter jupyter \
  && mkdir /home/jupyter \
  && chown -R jupyter:jupyter /home/jupyter

WORKDIR /home/jupyter

USER jupyter

ENTRYPOINT [ "jupyter", "notebook", "--port", "8888" ]